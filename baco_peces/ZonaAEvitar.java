package AI_6.baco_peces;

/**
 * Objetos situados que poseen una propiedad que indica su alcance y el tiempo restante de vida de la zona.
 * 
 */
public class ZonaAEvitar extends Objeto {
    protected double radio;
    protected int tiempoRestante = 500;

    public ZonaAEvitar(double _x, double _y, double _radio) {
        posX = _x;
        posY = _y;
        radio = _radio;
    }

    /**
     * 
     * @return
     */
    public double getRadio() {
        return radio;
    }

    /**
     * Decrementa el tiempo de vida restante
     */
    public void Actualizar() {
        tiempoRestante--;
    }

    /**
     * Devuelve verdadero si el tiempo restante ha llegado a 0.
     * @return
     */
    public boolean estaMuerto() {
        return tiempoRestante <= 0;
    }
}
