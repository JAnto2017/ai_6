package AI_6.baco_peces;

/**
 * Puede utilizarse a la vez por los objetos y los agentes.
 * posX, posY indican las coordenadas del objeto.
 * 
 */
public class Objeto {
    public double posX;
    public double posY;

    public Objeto() {}

    public Objeto(double _x, double _y) {
        posX = _x;
        posY = _y;
    }

    /**
     * Devuelve la distancia al cuadrado.
     * @param o
     * @return
     */
    public double DistanciaCuadrado(Objeto o) {
        return (o.posX - posX) * (o.posX - posX) + (o.posY - posY) * (o.posY - posY);
    }

    /**
     * Distancia exacta si es necesaria.
     * @param o
     * @return
     */
    public double Distancia(Objeto o) {
        return Math.sqrt(DistanciaCuadrado(o));
    }
}
