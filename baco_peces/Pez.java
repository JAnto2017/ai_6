package AI_6.baco_peces;

import java.util.ArrayList;

/**
 * Representa a los agentes-peces. Hereda de Objeto.
 * Se agregan, varias constantes:
 * => distancia recorrida en cada iteración PASO en una unidad arbitraria.
 * => distancia que indicca cuál es la zona de separación y su versión al cuadrado, para optimizar el cálculo.
 * => distancia que indica hasta dónde alcanza la zona de alineamiento y su versión al cuadrado.
 */
public class Pez extends Objeto {
    public static final double PASO = 3;
    public static final double DISTANCIA_MIN = 5;
    public static final double DISTANCIA_MIN_CUADRADO = 25;
    public static final double DISTANCIA_MAX = 40;
    public static final double DISTANCIA_MAX_CUADRADO = 1600;

    // atributos
    protected double velocidadX, velocidadY;

    // constructor
    public Pez(double _x,double _y,double _dir) {
        posX = _x;
        posY = _y;
        velocidadX = Math.cos(_dir);
        velocidadY = Math.sin(_dir);
    }

    // métodos
    /**
     * 
     * @return
     */
    public double getVelocidadX() {
        return velocidadX;
    }
    /**
     * 
     * @return
     */
    public double getVelocidadY() {
        return velocidadY;
    }

    /**
     * permite calcular la nueva posición del pez.
     * se trata de agregar a la posición actual la velocidad multiplicada por la longitud del desplazamiento.
     * 
     */
    protected void ActualizarPosicion() {
        posX += PASO * velocidadX;
        posY += PASO * velocidadY;
    }

    /**
     * Permite saber si existe algún otro pez cerca, en la zona de alineamiento
     * Utiliza distancias al cuadrado.
     * 
     * @param p
     * @return
     */
    protected boolean EnAlineacion(Pez p) {
        double distanciaCuadrado = DistanciaCuadrado(p);
        return (distanciaCuadrado < DISTANCIA_MAX_CUADRADO && distanciaCuadrado > DISTANCIA_MIN_CUADRADO);
    }

    /**
     * 
     * @param muroXMin
     * @param muroYMin
     * @param muroXMax
     * @param muroYMax
     * @return
     */
    protected double DistanciaAlMuro(double muroXMin, double muroYMin, double muroXMax, double muroYMax) {
        double min = Math.min(posX - muroXMin, posY - muroYMin);
        min = Math.min(min, muroXMax - posX);
        min = Math.min(min, muroYMax - posY);
        return min;
    }

    /**
     * Simplifica cálculo de las velocidades en los distintos casos que se presentan, agregamos una función que permite normalizarlas.
     * La velocidad de un pez sea constante en el tiempo. Se normaliza vector velocidad.
     * Comportamiento del pez:
     * 1) si hay un muro o una zona que se debe evitar en la zona muy próxima se evita.
     * 2) si hay un pez en la zona muy próxima, nos alejamos.
     * 3) si hay un pez en la zona próxima, nos alineamos con él.
     */
    protected void Normalizar() {
        double longitud = Math.sqrt(velocidadX * velocidadX + velocidadY * velocidadY);
        velocidadX /= longitud;
        velocidadY /= longitud;
    }

    /**
     * Normaliza el nuevo vector y devuelve verdadero si se detecta un muro.
     * 
     * @param muroXMin
     * @param muroYMin
     * @param muroXMax
     * @param muroYMax
     * @return
     */
    protected boolean EvitarMuros(double muroXMin, double muroYMin, double muroXMax, double muroYMax) {
        PararEnMuro(muroXMin,muroYMin,muroXMax,muroYMax);
        double distancia = DistanciaAlMuro(muroXMin, muroYMin, muroXMax, muroYMax);

        if (distancia < DISTANCIA_MIN) {
            CambiarDireccionMuro(distancia,muroXMin,muroYMin,muroXMax,muroYMax);
            Normalizar();
            return true;
        }
        return false;
    }

    /**
     * 
     * @param muroXMin
     * @param muroYMin
     * @param muroXMax
     * @param muroYMax
     */
    private void PararEnMuro(double muroXMin, double muroYMin, double muroXMax, double muroYMax) {
        if (posX < muroXMin) {
            posX = muroXMin;
        } else if (posY < muroYMin) {
            posY = muroYMin;
        } else if (posX > muroXMax) {
            posX = muroXMax;
        } else if (posY > muroYMax) {
            posY = muroYMax;
        }
    }

    /**
     * 
     * @param distancia
     * @param muroXMin
     * @param muroYMin
     * @param muroXMax
     * @param muroYMax
     */
    private void CambiarDireccionMuro(double distancia, double muroXMin, double muroYMin, double muroXMax, double muroYMax) {

        if (distancia == (posX - muroXMin)) {
            velocidadX += 0.3;
        } else if (distancia == (posY - muroYMin)) {
            velocidadY += 0.3;
        } else if (distancia == (muroXMax - posX)) {
            velocidadX -= 0.3;
        } else if (distancia == (muroYMax - posY)) {
            velocidadY -= 0.3;
        }
    }

    /**
     * Para evitar los obstáculos, buscaremos la zona que se debe evitar más próxima a la que estemos.
     * Si existe un obstáculo muy cerca de nosotros, calcularemos el vector dirección diff entre el pez y el obstáculo.
     * Aplicaremos una modificación del vector velocidad suprimiendo la mitad de este vector diff.
     * 
     * @param obstaculos
     * @return
     */
    protected boolean EvitarObstaculos(ArrayList<ZonaAEvitar> obstaculos) {

        if (!obstaculos.isEmpty()) {
            //búsqueda del obstáculo más cercano
            ZonaAEvitar obstaculoProximo = obstaculos.get(0);
            double distanciaCuadrado = DistanciaCuadrado(obstaculoProximo);

            for (ZonaAEvitar o : obstaculos) {
                if (DistanciaCuadrado(o) < distanciaCuadrado) {
                    obstaculoProximo = o;
                    distanciaCuadrado = DistanciaCuadrado(o);
                }
            }

            if (distanciaCuadrado < (4*obstaculoProximo.radio * obstaculoProximo.radio)) {
                //si colisiona, se calcula el vector diff
                double distancia = Math.sqrt(distanciaCuadrado);
                double diffX = (obstaculoProximo.posX - posX) / distancia;
                double diffY = (obstaculoProximo.posY - posY) / distancia;
                velocidadX = velocidadX - diffX / 2;
                velocidadY = velocidadY - diffY / 2;
                Normalizar();
                return true;
            }
        }
        return false;
    }

    /**
     * Para evitar peces demasiado próximos de manera flexible, se calcula el vector unitario entre el agente y el pez más próximo a él, que sustraeremos de su propia dirección. 
     * Terminaremos normalizando nuestro vector velocidad y devolveremos verdadero si hemos evitado un pez.
     * 
     * @param peces
     * @return
     */
    protected boolean EvitarPeces(Pez[] peces) {
        //búsqueda del pez mas cercano
        Pez p;
        if (!peces[0].equals(this)) {
            p = peces[0];
        } else {
            p = peces[1];
        }

        double distanciaCuadrado = DistanciaCuadrado(p);

        for (Pez pez : peces) {
            if (DistanciaCuadrado(pez) < distanciaCuadrado && !pez.equals(this)) {
                p = pez;
                distanciaCuadrado = DistanciaCuadrado(p);
            }
        }

        //evitar
        if (distanciaCuadrado < DISTANCIA_MIN_CUADRADO) {
            double distancia = Math.sqrt(distanciaCuadrado);
            double diffX = (p.posX - posX) / distancia;
            double diffY = (p.posY - posY) / distancia;
            velocidadX = velocidadX - diffX / 4;
            velocidadY = velocidadY - diffY / 4;
            Normalizar();
            return true; 
        }
        return false;
    }

    /**
     * Se busca en primer lugar todos los peces en nuestra zona de alineamiento. 
     * La nueva dirección del pez será una media entre la dirección de los demás peces y su dirección actual, para tener cierta fluidez en los movimientos.
     * Calcula la dirección media de los peces próximos y normalizar el vector final.
     * 
     * @param peces
     */
    protected void CalcularDireccionMedia(Pez[] peces) {
        double velocidadXTotal = 0;
        double velocidadYTotal = 0;
        int numTotal = 0;

        for (Pez p : peces) {
            if (EnAlineacion(p)) {
                velocidadXTotal += p.velocidadX;
                velocidadYTotal += p.velocidadY;
                numTotal++;
            }
        }

        if (numTotal >=  1) {
            velocidadX = (velocidadXTotal / numTotal + velocidadX) / 2;
            velocidadY = (velocidadYTotal / numTotal + velocidadY) / 2;
            Normalizar();
        }
    }

    /**
     * Actualizar los peces y se corresponde con su funcionamiento global.
     * Se busca en primer lugar evitar un muro, a continuación un obstáculo y por último un pez.
     * Si no hay nada que evitar, se pasa al comportamiento de alineamiento.
     * Una vez se ha calculado la nueva dirección, se calcula la nueva posición mediante ActualizarPosicion()
     * 
     * @param peces
     * @param obstaculos
     * @param ancho
     * @param alto
     */
    protected void Actualizar(Pez[] peces, ArrayList<ZonaAEvitar> obstaculos, double ancho, double alto) {
        if (!EvitarMuros(0, 0, ancho, alto)) {
            if (!EvitarObstaculos(obstaculos)) {
                if (!EvitarPeces(peces)) {
                    CalcularDireccionMedia(peces);
                }
            }
        }
        ActualizarPosicion();
    }
}
