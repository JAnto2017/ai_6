package AI_6.baco_peces;

import javax.swing.JFrame;

public class Aplicacion {
    public static void main(String[] args) {
        //creacion ventana

        JFrame ventana = new JFrame();
        
        ventana.setTitle("Banco de peces");
        ventana.setSize(600,400);
        ventana.setLocationRelativeTo(null);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.setResizable(false);

        //creación del contenido
        OceanoJPanel panel = new OceanoJPanel();
        ventana.setContentPane(panel);

        //visualización
        ventana.setVisible(true);
        panel.Ejecutar();
    }
}
