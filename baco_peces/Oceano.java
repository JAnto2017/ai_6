package AI_6.baco_peces;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Random;

/**
 * Banco de peces = océano virtual.
 * Se actualiza de manera asíncrona.
 * El océano puede avisar a la interfaz que la actualización ha terminado.
 * Se incluye array de peces y una lista de obstáculos.
 * Tiene generador aleatorio y dos atributos para indicar su tamaño (ancho y alto)
 * Se agrega un controlador para desencadenar el envío de la notificación.
 */
public class Oceano {
    protected Pez[] peces;
    protected ArrayList<ZonaAEvitar> obstaculos;
    protected Random generador;
    protected double ancho;
    protected double alto;
    private PropertyChangeSupport support;
    private int contador;

    //constructor
    public Oceano(int _numPeces, double _ancho, double _alto) {
        support = new PropertyChangeSupport(this);
        contador = 0;
        ancho = _ancho;
        alto = _alto;
        generador = new Random();
        obstaculos = new ArrayList<>();
        peces = new Pez[_numPeces];

        for (int i = 0; i < _numPeces; i++) {
            peces[i] = new Pez(generador.nextDouble() * ancho, generador.nextDouble() * alto, generador.nextDouble() * 2 * Math.PI);
        }
    }

    //métodos
    /**
     * 
     * @param pcl
     */
    public void AgregarChangeListener(PropertyChangeListener pcl) {
        support.addPropertyChangeListener(pcl);
    }
    /**
     * 
     * @param pcl
     */
    public void EliminarPropertyChangeListener(PropertyChangeListener pcl) {
        support.removePropertyChangeListener(pcl);
    }

    /**
     * Crea una nueva zona que evitar en las coordenadas indicadas, con el alcance solicitado y la agrega a la lista actual.
     * 
     * @param _posX
     * @param _posY
     * @param radio
     */
    public void AgregarObstaculo(double _posX,double _posY,double radio) {
        obstaculos.add(new ZonaAEvitar(_posX, _posY, radio));
    }

    /**
     * La actualización de los obstáculos consiste simplemente en pedir a cada zona que se actualice y a continuación que elimine las zonas que han alcanzado su final de vida.
     * 
     */
    protected void ActualizarObstaculos() {
        for (ZonaAEvitar obstaculo : obstaculos) {
            obstaculo.Actualizar();
        }
        obstaculos.removeIf(o -> o.estaMuerto());
    }

    /**
     * Se invoca, para cada pez, a su método de actualización.
     * 
     */
    protected void ActualizarPeces() {
        for (Pez p : peces) {
            p.Actualizar(peces, obstaculos, ancho, alto);
        }
    }

    /**
     * Solicita la actualización de todo el océano.
     * Se actualizan los obstáculos y a continuación, los peces.
     * Se avisa a los suscritos a través de fireProp... y la actualización del contador.
     * 
     */
    public void ActualizarOceano() {
        ActualizarObstaculos();
        ActualizarPeces();
        support.firePropertyChange("changed", this.contador, this.contador + 1);
        this.contador++;
    }

}
