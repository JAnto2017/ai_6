package AI_6.baco_peces;

import javax.swing.JPanel;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.Graphics;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Timer;
import java.util.TimerTask;

public class OceanoJPanel extends JPanel implements PropertyChangeListener, MouseListener{
    
    protected Oceano oceano;
    protected Timer timer;

    /**
     * Constructor fija un fondo azul claro y agrega el listener para las acciones del ratón.
     * 
     */
    public OceanoJPanel(){
        this.setBackground(new Color(150,255,255));
        this.addMouseListener(this);
    }

    /**
     * 
     */
    public void Ejecutar() {
        oceano = new Oceano(250, this.getWidth(), getHeight());
        oceano.AgregarChangeListener((PropertyChangeListener) this);

        TimerTask tarea = new TimerTask() {
            @Override
            public void run() {
                oceano.ActualizarOceano();                
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(tarea, 0, 15);
    }

    /**
     * Dibuja un pez 10 píxel de largo desde cabeza hasta cola
     * @param p
     * @param g
     */
    protected void DibujarPez(Pez p, Graphics g) {
        g.drawLine((int) p.posX, (int) p.posY, (int) (p.posX - 10 * p.velocidadX), (int) (p.posY - 10 * p.velocidadY));
    }

    /**
     * Dibuja zonas a evitar. Se traza círculo.
     * @param o
     * @param g
     */
    protected void DibujarObstaculo(ZonaAEvitar o, Graphics g) {
        g.drawOval((int)(o.posX - o.radio), (int) (o.posY - o.radio), (int) o.radio * 2, (int) o.radio * 2);
    }

    @Override
    public void update(Graphics g) {
        // TODO Auto-generated method stub
        super.update(g);
    }

    /**
     * relanza la visualización del panel
     * @param evt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.repaint();
    }

    /**
     * Dibuja peces y obstáculos
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Pez p : oceano.peces) {
            DibujarPez(p, g);
        }

        for (ZonaAEvitar o : oceano.obstaculos) {
            DibujarObstaculo(o, g);
        }
    }

    /**
     * Gestión del ratón.
     * Al hacer click en el Panel, se agrega un obstáculo en forma de círculo de 10 píxel de radio.
     * 
     */
    public void mouseClicked(MouseEvent e) {
        oceano.AgregarObstaculo(e.getX(), e.getY(), 10);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }
}
