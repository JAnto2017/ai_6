# SISTEMAS MULTIAGENTES

> Estos sistemas, permiten responder a una gran variedad de problemáticas. 
> Varios agentes, trabajarán de manera conjunta para la resolucion de problemas más complejos.

> Todas las técnicas clasificadas como **sistemas multiagentes** tiene como objetivo implementar esta inteligencia social, que se denomina **inteligencia distribuida**.
> Para ello, encontramos:
> * Un entorno.
> * Objetos fijos o no, que son los obstáculos o los puntos de interés.
> * Agentes, como comportamientos simples.
___

## Percepción del mundo

> Por *ejemplo*, si tomamos robots que se desplazan por la planta de un edificio, pueden conocer lo que ven, o tener un mapa registrado previamente de toda la planta con su posición.
___

### Percepción del mundo

> Los agentes deben escoger qué hacer en todo momento.
> Un individuo que reacciona directamente a sus percepciones sería un **agente reactivo**. Por el contrario, si reacciona tras reflexionar una decisión en función de conocimientos, se trata de un **agente cognitivo**.

### Toma de decisiones

> En la mayoría de los casos, se agrega también un **aspecto estocástico**: la repsencia de *algo de aleatoriedad* va a permitir a menudo que el sistema sea más fluido y más flexible.

### Cooperación y comunicación

> Podemos imaginar sistemas puramente reactivos que no realizan cambios en sus entorno. Se observan comportamientos de grupo por **emergencia**.
> Los individuos pueden, **comunicarse directamente** mediante mensajes.
> Los agentes pueden llegar a **negociar** entre sí para encontrar un consenso que será la solución adoptada.

### Capacidad del agente

> Pueden tener capacidades muy limitadas o bien una gran gama de acciones posibles.
> Los distintos agentes pueden aprender con el paso del tiempo o bien tener conocimientos fijos.
___

# Principales Algoritmos

> Se presentan cuatro algoritmos.
___

## Algoritmos de manadas

> Simula **comportamientos de manadas** o de grupos.
> Comportamientos, vinculados a la presencia de otros individuos en su proximidad:
> * Un individuo muy cercano va a provocar un comportamiento para evitarse mutuamente para evitar invadir a otro individuo; es el **comportamiento de separación**.
> * Un individuo próxima modifica la dirección de la criatura, que tiene tendencia a alinearse en la dirección de su vecino; es el **comportamiento de alineamiento**.
> * Un individuo a una distancia media va a provocar un acercamiento. En efecto, si una criatura ve a otra, irá hacia ella; es el **comportamiento de cohesión**.

## Optimización por colonia de hormigas

> Al inicio, el entorno está virgen. Las hormigas virtuales van a recorrer el espacio aleatoriamente, hasta encontrar una solución.
> La hormiga va a volver a su punto de partida depositando feromonas.

> La posibilidad de seguir una dirección depende de varios criterios:
> * Las posibles direcciones.
> * La dirección de la que viene la hormiga, para no dar media vuelta.
> * Las pistas de feromonas a su alrededor.
> * Otros metaheurísticos.

> Las feromonas depositadas deben, en sí, ser proporcionales a la calidad de la solución o a su longitud. Pueden, ser constante. Deben evaporarse, **tasa de evaporación**.

## Sistemas inmunitarios artificiales

> Los sistemas inmunitarios artificiales hacen evolucionar, en un entorno determinado, diferentes agentes de defensa. Cada uno conoce un conjunto de **amenazas**, que sabe detectar y combatir. Estos agentes pueden crearse aleatoriamente al inicio.

> Los agentes pueden desplazarse y **aprender** los unos de los otros, lo que permite obtener respuestas mejor adaptadas y más rápidas, incluso contra ataques desconocidos.

## Autómatas Celulares

> El entorno es una malla regular. Cada agente se sitúa sobre una de las casillas de la malla y no puede cambiar. Posee varios estados posibles.

> * Las células tienen solamente dos estados: vivas o muertas (representados por negro o blanco).
> * Actualizan su estado en función de sus 8 vecinas inmediatas.

## Implementación

> Implementación de varios ejemplos. Por su funcionamiento, estos algoritmos son principalmente gráficos.

> Se crearán dos clases gráficas para cada simulación.
> - Una que hereda de *JPanel* que gestiona la visualización gráfica y la actuación de la aplicación.
> - Una que contiene el *main* y que ejecuta la ventana principal agregándole el panel previamente creado.

> La primera aplicación es una simulación de un banco de peces, inspirado en los *boids de Reynolds* en dos dimensiones.
___

# Resultados Obtenidos - Banco de Peces

> Los peces se aproxima unos a otros para crear grupos. 
> Se desplazan en bancos y evitan los obstáculos.
> El comportamiento en *banco de peces* es totalmente emergente, estando las reglas codificadas basadas en su vecindario próximo o muy próximo. A diferencia de los *boids de Reynolds*, nuestros peces no tienen zona de coherencia, lo que explica que el grupo se escinda en ocasiones.
___
> Banco de peces formando grupos sin obstáculos.
![Banco de peces I](bando_peces_1.png "Bando de peces sin obstáculos")
___
> Banco de peces formando grupos con obstáculos (círculos):
![Banco de peces II](bando_peces_2.png "Bando de peces con obstáculos")
___
# Resultados Obtenidos - Recogida Selectiva

> Aparte de algunas simulaciones en las que los *agentes* retiran todos los residuos de un tipo, lo que no permite volver a depositarlos posteriormente, todas las demás situaciones *convergen hacia la presencia de tres montones*, uno por cada tipo.

> Situación inicial. **Residuos** rodeados, **agentes** pequeños cuadrados solitarios.

![](recog_selec_0.png)
___

> Pasados varios segundos a minutos, los **agentes** han desplazado los **residuos**.

![](recog_selec_2.png)

![](recog_selec_3.png)
___

> Existen únicamente tres montones, uno por cada tipo. Existe un montón más pequeño que los otros dos. Esto es debido a que los residuos se reparten aleatoriamente entre los distintos tipos.

![](recog_selec_1.png)

___

# Resultados Obtenidos - Juego de la Vida

> Tras la segunda iteración, todas las células asiladas desaparecen. En las siguientes iteraciones, tienen lugar "explosiones" en la malla, dejando tras de sí estructuras estables u oscilantes, en su mayoría de periodo 2.
> De vez en cuando aparecen también algunos barcos que se desplazan hasta encontrar otra estructura.
> Tras varias generaciones, solo permanecen las estructuras estables y oscilantes. He aquí, por ejemplo, el estado final de una simulación en su iteración N.

![](juegoVida.png)

___

> Parpadeadores - a menudo situados en cruz o en círculo en función del tiempo, sapos y con menos frecuencia, barcos.

> En raras ocasiones pueden aparecer otras estructuras.
> El usuario puede, en todo momento, agregar o eliminar células vivas. La malla va a evolucionar, a continuación, hasta estabilizarse de nuevo.

___

# RESUMEN

Los sistemas **multiagentes** permiten resolver un gran número de problemas, tanto en la simulación de multitudes, en la planificación y la búsqueda de rutas o en la simulación de problemas complejos, para comprenderlos mejor y ayudar en su estudio.

La solución aparece mediante emergencia, y no según un plan preprogramado. Por lo tanto, los sistemas multiagentes contienen un entorno en el que se encuentran objetos y agentes. Existen unas pocas reglas y cada problema tiene una o varias modelizaciones posibles.

En todos los casos, es la multiplicación de agentes y los vínculos que se establece entre ellos, directamente por comunicación o indirectamente por **estimergia** (o incluso sin comunicación entre ellos), lo que permite hacer emerger la solución. Se observa la potencia de la inteligencia distribuida.

