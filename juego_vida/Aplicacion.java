package AI_6.juego_vida;

import javax.swing.JFrame;

/**
 * En primer lugar se crea una ventana.
 * Incluir el panel previamente creado y por último ejecutar la simulación.
 * 
 */
public class Aplicacion {
    public static void main(String[] args) {
        // Crear ventama
        JFrame ventana = new JFrame();
        ventana.setTitle("Juego de la vida");
        ventana.setSize(600,400);
        ventana.setLocationRelativeTo(null);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.setResizable(false);

        //Crear contenido
        JuegoDeLaVidaJPanel panel = new JuegoDeLaVidaJPanel();
        ventana.setContentPane(panel);

        //visualizar
        ventana.setVisible(true);
        panel.Ejecutar();
    }
}
