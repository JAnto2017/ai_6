package AI_6.juego_vida;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JPanel;

/**
 * Hereda de JPanel e implementa las interfaces Propert...
 * Esta vez hacer click con botón derecho permitirá poner la aplicación en pausa o relanzarla y los clics con el botón izquierdo permitirá modificar el estado de la célula situada bajo el ratón.
 * 
 */
public class JuegoDeLaVidaJPanel extends JPanel implements PropertyChangeListener, MouseListener {

    Timer timer;
    boolean enCurso = false;
    Malla malla;
    TimerTask tarea;

    /**
     * constructor configurar color de fondo y después suscribirse a los clics del ratón.
     * 
     */
    public JuegoDeLaVidaJPanel() {
        this.setBackground(Color.WHITE);
        this.addMouseListener(this);
    }

    /**
     * Inicializa la malla, con una densidad de células vivas del 1o%
     * se abonará a la malla y ejecutará el timer que permite actualizar la representación gráfica cada 500 ms.
     * 
     */
    public void Ejecutar() {
        malla = new Malla(this.getWidth() / 3, getHeight() / 3, 0.1);
        malla.AgregarChangeListener(this);
        timer = new Timer();
        tarea = new TimerTask() {
            @Override
            public void run() {
                malla.Actualizar(true);
            }
        };

        timer.scheduleAtFixedRate(tarea, 0, 500);
        enCurso = true;
    }

    /**
     * Para la actualización, basta con reiniciar la visualización de la malla.
     * 
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
       this.repaint(); 
    }

    /**
     * Dibujar las células vivas, con cuadrados de 3 píxeles de lado.
     * 
     * @param g
     * @param i
     * @param j
     */
    public void DibularCelula(Graphics g, int i, int j) {
        g.fillRect(3*i-1, 3*j-1, 3, 3);
    }

    /**
     * Solicita el pintado de cada célula si está viva.
     * 
     */
    @Override
    protected void paintComponent(Graphics g) {        
        super.paintComponent(g);
        for (int i = 0; i < malla.ancho; i++) {
            for (int j = 0; j < malla.alto; j++) {
                if (malla.contenido[i][j]) {
                    DibularCelula(g, i, j);
                }
            }
        }
    }

    /**
     * clic izquierdo, se cambia el estado de la célula situada debajo del ratón.
     * clic derecho, se pone en pausa o se relanza la simulación.
     * 
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            malla.CambiarEstado(e.getX()/3, e.getY()/3);
            malla.Actualizar(false);
        } else if(e.getButton() == MouseEvent.BUTTON3) {
            if (enCurso) {
                timer.cancel();
                timer = null;
            } else {
                timer = new Timer();
                tarea = new TimerTask() {
                    @Override
                    public void run() {
                        malla.Actualizar(true);
                    }
                };
                timer.scheduleAtFixedRate(tarea, 0, 500);
            }
            enCurso = !enCurso;
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) { }
    @Override
    public void mouseExited(MouseEvent e) { }
    @Override
    public void mousePressed(MouseEvent e) { }
    @Override
    public void mouseReleased(MouseEvent e) { }
}
