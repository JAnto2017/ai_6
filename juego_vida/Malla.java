package AI_6.juego_vida;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Random;

/**
 * Se dibuja tras cada actualización, de modo que aquí también vamos a utilizar el patrón de diseño Observador.
 * La malla, posee un ancho y un largo definido en número de células y un array de dos dimensiones que contiene todas las células, que son simplemente valores booleanos que indican si la célula situada en dicha casilla está viva o no.
 * 
 */
public class Malla {
    protected int ancho, alto;
    protected boolean[][] contenido;
    private PropertyChangeSupport support;
    private int numIteraciones;

    public void AgregarChangeListener(PropertyChangeListener pcl) {
        support.addPropertyChangeListener(pcl);
    }

    //constructor
    public Malla(int _ancho,int _alto,double _densidad){
        ancho = _ancho;
        alto = _alto;
        Random generador = new Random();
        support = new PropertyChangeSupport(this);
        numIteraciones = 0;

        contenido = new boolean[ancho][alto];

        for (int i = 0; i < ancho; i++) {
            for (int j = 0; j < alto; j++) {
                if (generador.nextDouble() < _densidad) {
                    contenido[i][j] = true;
                }
            }
        }
    }

    /**
     * Permite cambiar el estado de una célula concreta y otro que permita saber el número de células vecinas vivas.
     * Para cambiar el estado de la célula basta con invertir valor booleano.
     * 
     * @param fila
     * @param columna
     */
    public void CambiarEstado(int fila,int columna) {
        contenido[fila][columna] = !contenido[fila][columna];
    }

    /**
     * Hay que prestar atención a no salirse de la malla, de modo que verificamos en primer lugar las coordenadas mínimas y máximas respecto a las dimensiones del entorno.
     * Es preciso, también, no contar la célula del centro.
     * 
     * @param columna
     * @param fila
     */
    public int NumVecinosVivos(int columna,int fila) {
        int i_min = Math.max(0, columna-1);
        int i_max = Math.min(ancho-1, columna+1);
        int j_min = Math.max(0, fila-1);
        int j_max = Math.min(alto-1, fila+1);
        int num = 0;

        for (int i = i_min; i <= i_max; i++) {
            for (int j = j_min; j <= j_max; j++) {
                if (contenido[i][j] && !(i==columna && j==fila)) {
                    num++;
                }
            }
        }
        return num;
    }

    /**
     * El valor booleano como parámetro indica si se quiere actualizar realmente la aplicación o simplemente si se quiere producir el evento para recuperar el estado actual de las células.
     * Este último se utiliza cuando el usuario quiere cambiar el estado de una célula para actualizar la visualización.
     * 
     * @param conAplicacion
     */
    public void Actualizar(boolean conAplicacion) {
        if (conAplicacion) {
            boolean[][] nuevaMalla = new boolean[ancho][alto];

            for (int i = 0; i < ancho; i++) {
                for (int j = 0; j < alto; j++) {
                    int num = NumVecinosVivos(i, j);
                    if (num == 3 || (num == 2 && contenido[i][j])) {
                        nuevaMalla[i][j] = true;
                    }
                }
            }
            contenido = nuevaMalla;
        }
        support.firePropertyChange("changed", this.numIteraciones, this.numIteraciones + 1);
        this.numIteraciones++;
    }

}
