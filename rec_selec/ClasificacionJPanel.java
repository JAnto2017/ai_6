package AI_6.rec_selec;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

import java.awt.event.MouseListener;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.Graphics;
/**
 * Creamos un JPanel para mostrar nuestros resisduos y nuestros agentes.
 * El panel poseerá:
 * Un timer que lanzará la actualización del entorno cada 10 ms.
 * Un booleano que indica si la aplicación está en pausa o no.
 * Una referencia hacia el entorno para evitar tener que invocar getInstance demasiado a menudo.
 * 
 * 
 */
public class ClasificacionJPanel extends JPanel implements PropertyChangeListener, MouseListener {
    
    Timer timer;
    TimerTask tarea;
    boolean enCurso = false;
    Entorno entorno;

    /**************************************************************************************************
     * 
     */
    public ClasificacionJPanel() {
        this.setBackground(Color.WHITE);
        this.addMouseListener(this);
    }

    /**************************************************************************************************
     * Inicializa el entorno, con 50 residuos de tres tipos y 30 agentes y abonarse a las actualizaciones.
     * 
     */
    public void Ejecutar() {
        entorno = Entorno.getInstance();
        entorno.Inicializar(50, 30, getWidth(), getHeight(), 3);
        entorno.AgregarChangeListener(this);
    }

    /**************************************************************************************************
     * Comprueba si la aplicación está en curso o no, en caso afirmativo, se detiene el timer.
     * En caso contrario, se lanza un nuevo timer.
     * La aplicación está en pausa tras su arranque.
     * 
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (enCurso) {
            // se detiene el timer
            timer.cancel();
            timer = null;
            enCurso = false;
        } else {
            // se lanza el timer
            timer = new Timer();
            tarea = new TimerTask() {
                @Override
                public void run() {
                    entorno.Actualizar();
                }
            };

            timer.scheduleAtFixedRate(tarea, 0, 10);
            enCurso = true;
        }
    }
    /**************************************************************************************************
     * 
     */
    @Override
    public void mouseEntered(MouseEvent e) {}
    /**************************************************************************************************
     * 
     */
    @Override
    public void mouseExited(MouseEvent e) {}
    /**************************************************************************************************
     * 
     */
    @Override
    public void mousePressed(MouseEvent e) {}
    /**************************************************************************************************
     * 
     */
    @Override
    public void mouseReleased(MouseEvent e) {}

    /**************************************************************************************************
     * Se invoca desde el entorno cuando este se actualiza.
     * Se pide a la interfaz que redibuje gracias a repaint.
     * Se agrega visualización por consola.
     * 
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.repaint();
        int agentesCargados = 0;
        
        for (AgenteClasificacion a : entorno.agentes) {
            if (a.estaCargado()) {
                agentesCargados++;
            }
        }

        System.out.println(entorno.residuos.size() + " - " + agentesCargados);
    }

    /**************************************************************************************************
     * Pintado de un agente.
     * Cuadrado de 3 píxel de lado, gris si está cargado el agente y negro en caso contrario.
     * 
     * @param agente
     * @param g
     */
    public void DibujarAgente(AgenteClasificacion agente, Graphics g) {
        if (agente.estaCargado()) {
            g.setColor(Color.GRAY);
        } else {
            g.setColor(Color.BLACK);
        }

        g.fillRect((int) agente.posX - 1, (int) agente.posY - 1, 3, 3);
    }
    
    /**************************************************************************************************
     * Color que depende del tipo de montón.
     * Orden: rojo, verde, azul.
     * cuadrado de 3 píxel de lado para representar el centro
     * círculo parcialmente transparente indicando la zona de influencia del montón - aumenta con el tamaño del montón -
     * 
     * @param r
     * @param g
     */
    public void DibujarResiduo(Residuo r, Graphics g) {
        // selección del color
        Color color;

        switch (r.tipo) {
            case 1:
                color = Color.RED;
                break;
            case 2:
                color = Color.GREEN;
                break;
            default:
                color = Color.BLUE;                
        }

        g.setColor(color);
        // Base: cuadrado
        g.fillRect((int) r.posX - 1, (int) r.posY - 1, 3, 3);
        // zona de influencia(redonda)
        color = new Color(color.getRed(), color.getGreen(), color.getBlue(), 50);
        g.setColor(color);
        int zona = r.ZonaInfluencia();
        g.fillOval((int) r.posX - zona, (int) r.posY - zona, zona * 2, zona * 2);
    }

    /**************************************************************************************************
     * Pide la visualización de cada agente y luego la de cada residuo.
     * 
     */
    @Override
    protected void paintComponent(Graphics g) {        
        super.paintComponent(g);

        for (AgenteClasificacion agente : entorno.agentes) {
            DibujarAgente(agente, g);
        }

        for (Residuo residuo : entorno.residuos) {
            DibujarResiduo(residuo, g);
        }
    }
}
