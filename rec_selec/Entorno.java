package AI_6.rec_selec;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Random;

/**
 * Para poder indicar a la interfaz que hay disponible una actualización, utilizaremos el patrón diseño Observador.
 * Esta clase es un singleton, de esta manera, todos los agentes pueden obtener una referencia a ella mediante el método getInstance.
 * 
 */
public class Entorno {

    protected Random generador;
    protected double ancho, alto;
    protected ArrayList<Residuo> residuos;
    protected ArrayList<AgenteClasificacion> agentes;
    protected int numIteraciones = 0;
    private PropertyChangeSupport support;

    // gestion del singleton
    private static Entorno instance;

    /**************************************************************************************************
     * 
     * @return
     */
    public static Entorno getInstance() {
        if (instance == null) {
            instance = new Entorno();
        }
        return instance;
    }

    /**************************************************************************************************
     * 
     * @param pcl
     */
    public void AgregarChangeListener(PropertyChangeListener pcl) {
        support.addPropertyChangeListener(pcl);
    }

    /**************************************************************************************************
     * Inicializa ambas listas y el generador aleatorio.
     * 
     */
    private Entorno() {
        residuos = new ArrayList<>();
        agentes = new ArrayList<>();
        generador = new Random();
        support = new PropertyChangeSupport(this);
    }

    /**************************************************************************************************
     * Permite poblar el entorno, creando residuos y los agentes con un generador aleatorio.
     * 
     * @param _numResiduos
     * @param _numAgentes
     * @param _ancho
     * @param _alto
     * @param _numTiposResiduos
     */
    public void Inicializar(int _numResiduos,int _numAgentes,double _ancho,double _alto,int _numTiposResiduos) {
        ancho = _ancho;
        alto = _alto;
        residuos.clear();

        for (int i = 0; i < _numResiduos; i++) {
            Residuo residuo = new Residuo(generador.nextDouble() * ancho, generador.nextDouble() * alto, generador.nextInt(_numTiposResiduos));
            residuos.add(residuo);
        }
        
        agentes.clear();

        for (int i = 0; i < _numAgentes; i++) {
            AgenteClasificacion agente = new AgenteClasificacion(generador.nextDouble() * ancho, generador.nextDouble() * alto);
            agentes.add(agente);
        }
    }

    /**************************************************************************************************
     * 
     * @return
     */
    public double getAncho() {
        return ancho;
    }

    /**************************************************************************************************
     * 
     * @return
     */
    public double getAlto() {
        return alto;
    }

    /**************************************************************************************************
     * Permite indicar que un agente ha depositado un nuevo elemento en un montón existente.
     * 
     * @param r
     */
    public void DepositarResiduo(Residuo r) {
        r.AumentarTamanio();
    }

    /**************************************************************************************************
     * Comprueba el tamaño del montón.
     * si hay un solo elemento, es el que recuperará el agente y eliminará el montón de la lista de residuos.
     * 
     * @param r
     * @return
     */
    public Residuo TomarResiduo(Residuo r) {
        if (r.tamanio == 1) {
            residuos.remove(r);
            return r;
        } else {
            r.DisminuirTamanio();
            Residuo carga = new Residuo(r);
            return carga;
        }
    }

    /**************************************************************************************************
     * Actualiza el entorno.
     * Para cada agente se le pide actualizar su dirección y después su posición.
     * Se incrementa el número de iteraciones.
     * Antes de incrementar el número de iteraciones, se indica que ha habido cambios y se avisa a los observadores.
     * 
     */
    public void Actualizar() {
        for (AgenteClasificacion agente : agentes) {
            agente.ActualizarPosicion();
            agente.ActualizarDireccion(residuos);
        }

        support.firePropertyChange("changed", numIteraciones, numIteraciones+1);
        numIteraciones++;

        if (numIteraciones % 500 == 0) {
            Collections.reverse(residuos);
        }
    }
}
