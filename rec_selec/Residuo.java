package AI_6.rec_selec;

/**
 * Hereda de la clase Objeto, posee dos atributos.
 * uno indica el tipo de residuo, valor entero.
 * otro indica el tamaño del montón en número de residuos depositados en este sitio.
 * se agrega constante DIS... que permite saber velocidad probabilidad de coger un elemento de un montón disminuye con el tamaño.
 * 
 */
public class Residuo extends Objeto {
    protected final static double DISMINUCION = 0.6;
    protected int tipo;
    protected int tamanio = 1;

    public int getTipo() {
        return tipo;
    }

    public int getTamanio() {
        return tamanio;
    }

    /**
     * --------------------------------------------------------------- constructor
     * @param _posX
     * @param _posY
     * @param _tipo
     */
    public Residuo(double _posX,double _posY,int _tipo){
        tipo=_tipo;
        posX=_posX;
        posY=_posY;
    }

    /**
     * --------------------------------------------------------------- constructor
     * @param r
     */
    public Residuo(Residuo r) {
        posX = r.posX;
        posY = r.posY;
        tipo = r.tipo;
    }

    /**
     * cada montón tiene zona influenciable representada por su alcance.
     * cuanto más grande sea un montón más atraerá a los agentes a su alrededor.
     * @return
     */
    public int ZonaInfluencia() {
        return 10 + 8 * (tamanio - 1);
    }

    protected void AumentarTamanio() {
        tamanio++;
    }

    protected void DisminuirTamanio() {
        tamanio--;
    }

    /**
     * Permite indicar la probabilidad de tomar un elemento de un montón.
     * @return
     */
    public double ProbabilidadDeTomar() {
        return Math.pow(DISMINUCION, tamanio - 1);
    }
}
