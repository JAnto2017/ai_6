package AI_6.rec_selec;

import javax.swing.JFrame;

public class Aplicacion {
    public static void main(String[] args) {
        
        // creación de la ventana
        JFrame ventana = new JFrame();
        ventana.setTitle("Recogida selectiva");
        ventana.setSize(600,400);
        ventana.setLocationRelativeTo(null);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.setResizable(false);

        // creación del contenido
        ClasificacionJPanel panel = new ClasificacionJPanel();
        ventana.setContentPane(panel);

        // visualización
        ventana.setVisible(true);
        panel.Ejecutar();
    }
}
