package AI_6.rec_selec;

/**
 * Los residuos son objetos situados en el mundo, como los agentes.
 * 
 */
public class Objeto {
    public double posX, posY;

    public Objeto() {}
    public Objeto(double _x,double _y) {
        posX = _x;
        posY = _y;
    }

    /**
     * 
     * @param o
     * @return
     */
    public double Distancia(Objeto o) {
        return Math.sqrt(DistanciaCuadrado(o));
    }

    /**
     * 
     * @param o
     * @return
     */
    public double DistanciaCuadrado(Objeto o) {
        return (o.posX - posX) * (o.posX - posX) + (o.posY - posY) * (o.posY - posY);
    }
}
