package AI_6.rec_selec;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Posee atributos adicionales:
 * La carga actualmente transportada, de tipo Residuo
 * Vector de velocidad expresado por sus coordenadas.
 * valor booleano que indica si está actualmente ocupado
 * 
 * Los agentes tendrán que acceder regularmente al entorno que los contiene.
 */
public class AgenteClasificacion extends Objeto {
    protected final static double PASO = 3;
    protected final static double PROB_CAMBIO_DIRECCION = 0.05;
    protected Residuo carga;
    protected double velocidadX, velocidadY;
    protected boolean ocupado = false;

    //constructor
    public AgenteClasificacion(double _posX,double _posY) {
        posX = _posX;
        posY = _posY;
        velocidadX = Entorno.getInstance().generador.nextDouble() - 0.5;
        velocidadY = Entorno.getInstance().generador.nextDouble() - 0.5;
        Normalizar();
    }

    /**
     * Permite normalizar los vectores de velocidad.
     * 
     */
    protected void Normalizar() {
        double longitud = Math.sqrt(velocidadX * velocidadX + velocidadY * velocidadY);
        velocidadX /= longitud;
        velocidadY /= longitud;
    }

    /**
     * Indica si el agente está cargado o no.
     * @return
     */
    public boolean estaCargado() {
        return carga != null;
    }

    /**
     * Actualiza la posición.
     * utiliza las coordenadas máximas del espacio, mediante el acceso al entorno.
     * Las posiciones son incrementos de la velocidad multiplicada por el paso y se verifica que no está fuera de la zona autorizada.
     * 
     */
    public void ActualizarPosicion() {
        posX += PASO * velocidadX;
        posY += PASO * velocidadY;
        double ancho = Entorno.getInstance().getAncho();
        double alto = Entorno.getInstance().getAlto();

        if (posX < 0) {
            posX = 0;
        } else if (posX > ancho) {
            posX = ancho;
        }

        if (posY < 0) {
            posY = 0;
        } else if (posY > alto) {
            posY = alto;
        }                
    }

    /**
     * La nueva dirección se normaliza para mantener una velocidad constante.
     * 
     * @param residuos
     */
    protected void ActualizarDireccion(ArrayList<Residuo> residuos) {
        // Dónde ir ?
        ArrayList<Residuo> enZona = new ArrayList<>();
        enZona.addAll(residuos);
        enZona.removeIf(r -> (Distancia(r) > r.ZonaInfluencia()));

        Collections.sort(enZona, (Residuo r1, Residuo r2) -> (Distancia(r1) < Distancia(r2) ? -1 : 1));

        Residuo objetivo = null;

        if (carga != null) {
            enZona.removeIf(r -> r.tipo != carga.tipo);
        }

        if (!enZona.isEmpty()) {
            objetivo = enZona.get(0);
        }

        // Tenemos un objetivo ?
        if (objetivo == null || ocupado) {
            // Desplazamiento aleatorio
            if (Entorno.getInstance().generador.nextDouble() < PROB_CAMBIO_DIRECCION) {
                velocidadX = Entorno.getInstance().generador.nextDouble() - 0.5;
                velocidadY = Entorno.getInstance().generador.nextDouble() - 0.5;
            }
            if (ocupado && objetivo == null) {
                ocupado = false;
            }
        } else {
            // ir al objetivo
            velocidadX = objetivo.posX - posX;
            velocidadY = objetivo.posY - posY;
            // objetivo alcanzado ?
            if (Distancia(objetivo) < PASO) {
                if (carga == null) {
                    if (Entorno.getInstance().generador.nextDouble() < objetivo.ProbabilidadDeTomar()) {
                        carga = Entorno.getInstance().TomarResiduo(objetivo);
                    }
                } else {
                    Entorno.getInstance().DepositarResiduo(objetivo);
                    carga = null;
                }
                ocupado = true;
            }
        }
        Normalizar();
    }
}
